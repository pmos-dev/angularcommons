import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonsVisibilityService } from './services/commons-visibility.service';
import { CommonsFocusService } from './services/commons-focus.service';

@NgModule({
		imports: [
				CommonModule
		],
		declarations: [
		],
		exports: [
		]
})
export class NgxAngularCommonsBrowserModule {
	static forRoot(): ModuleWithProviders<NgxAngularCommonsBrowserModule> {
		return {
				ngModule: NgxAngularCommonsBrowserModule,
				providers: [
						CommonsVisibilityService,
						CommonsFocusService
				]
		};
	}
}
