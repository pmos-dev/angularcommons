import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable()
export class CommonsMultiPromiseCompletionService {
	private onActiveEmitters: Map<string, EventEmitter<number>> = new Map<string, EventEmitter<number>>();
	private onCompletedEmitters: Map<string, EventEmitter<void>> = new Map<string, EventEmitter<void>>();
	private counts: Map<string, number> = new Map<string, number>();
	
	public setup(name: string) {
		if (!this.onActiveEmitters.has(name)) {
			this.onActiveEmitters.set(name, new EventEmitter<number>(true));
			this.onCompletedEmitters.set(name, new EventEmitter<void>(true));
			this.counts.set(name, 0);
		}
	}

	public activeObservable(name: string): Observable<number> {
		this.setup(name);
		return this.onActiveEmitters.get(name)!;
	}

	public completedObservable(name: string): Observable<void> {
		this.setup(name);
		return this.onCompletedEmitters.get(name)!;
	}
	
	private increment(name: string): void {
		let existing: number = this.counts.get(name)!;
		existing++;

		this.counts.set(name, existing);
		this.onActiveEmitters.get(name)!.emit(existing);
	}
	
	private decrement(name: string): void {
		let existing: number = this.counts.get(name)!;
		if (existing > 0) existing--;

		this.counts.set(name, existing);

		this.onActiveEmitters.get(name)!.emit(existing);
		if (existing === 0) this.onCompletedEmitters.get(name)!.emit();
	}

	public async wrap<T>(
			name: string,
			promise: Promise<T>
	): Promise<T> {
		this.setup(name);

		return new Promise(
				(
						resolve: (_: T) => void,
						reject: (_: Error) => void
				): void => {
					this.increment(name);
					promise
							.then((outcome: T): void => {
								resolve(outcome);
							})
							.catch((error: Error): void => {
								reject(error);
							})
							.finally((): void => {
								this.decrement(name);
							});
				}
		);
	}

	public async wrapAll<T>(
			name: string,
			promises: Promise<T>[]
	): Promise<T[]> {
		this.setup(name);
		
		return Promise.all(
				promises
						.map((promise: Promise<T>): Promise<T> => {
							return this.wrap(name, promise);
						})
		);
	}
}
