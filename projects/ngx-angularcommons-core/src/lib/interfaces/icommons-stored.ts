export interface ICommonsStored {
	key: string;
	value: string|number|boolean|undefined|any;
}
