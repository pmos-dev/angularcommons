/*
 * Public API Surface of ngx-angularcommons-core
 */

export * from './lib/components/commons.component';
export * from './lib/components/commons-manual-change-detection.component';

export * from './lib/interfaces/icommons-stored';

export * from './lib/services/commons-status.service';
export * from './lib/services/commons-storage.service';
export * from './lib/services/commons-ui-parent-child-component.service';
export * from './lib/services/commons-multi-promise-completion.service';

export * from './lib/ngx-angularcommons-core.module';
