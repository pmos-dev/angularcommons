import { CommonsType } from 'tscommons-core';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { CommonsCacheService } from '../services/commons-cache.service';

import { ECommonsCacheAge } from '../enums/ecommons-cache-age';

type TAges = {
		[age in ECommonsCacheAge]: number;
};

export abstract class CommonsNamespacedSubCache {
	private ages: TAges;

	constructor(
			private ns: string,
			configService: CommonsConfigService,
			private cacheService: CommonsCacheService
	) {
		const ages: {} = CommonsType.assertObject(configService.getObject<TAges>('storage', 'cache_ages'));
		
		this.ages = {
				[ECommonsCacheAge.SHORT]: CommonsType.assertNumber(ages[ECommonsCacheAge.SHORT]),
				[ECommonsCacheAge.MEDIUM]: CommonsType.assertNumber(ages[ECommonsCacheAge.MEDIUM]),
				[ECommonsCacheAge.LONG]: CommonsType.assertNumber(ages[ECommonsCacheAge.LONG])
		};
	}

	protected get<T>(resolver: string, aspect: string|undefined, uid: string[]|string|undefined): T|undefined {
		const existing: any|undefined = this.cacheService.fetchNs(
				`${this.ns}-cache`,
				`${resolver}:${aspect === undefined ? '' : aspect}`,
				uid === undefined ? '' : uid
		);
		if (existing === undefined) return undefined;
	
		const typecast: T = existing as T;
		return typecast;
	}

	protected store<T>(resolver: string, aspect: string|undefined, uid: string[]|string|undefined, data: T, age: ECommonsCacheAge): void {
		this.cacheService.storeNs(
				`${this.ns}-cache`,
				`${resolver}:${aspect === undefined ? '' : aspect}`,
				uid === undefined ? '' : uid,
				data,
				this.ages[age]
		);
	}

	protected delete(resolver: string, aspect: string|undefined, uid: string[]|string|undefined): void {
		this.cacheService.deleteNs(
				`${this.ns}-cache`,
				`${resolver}:${aspect === undefined ? '' : aspect}`,
				uid === undefined ? '' : uid
		);
	}

	protected flushAspect(resolver: string, aspect: string|undefined): void {
		this.cacheService.flushNs(
				`${this.ns}-cache`,
				`${resolver}:${aspect === undefined ? '' : aspect}`
		);
	}

	//------------------------------------------------------------------------------------

	public flush(): void {
		this.cacheService.flushNs(`${this.ns}-cache`);
	}
}
