export type TCommonsCacheGetter<K, V> = (_: K) => V|undefined;
