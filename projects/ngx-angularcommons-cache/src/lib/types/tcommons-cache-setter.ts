export type TCommonsCacheSetter<K, V> = (_k: K, _v: V) => void;
