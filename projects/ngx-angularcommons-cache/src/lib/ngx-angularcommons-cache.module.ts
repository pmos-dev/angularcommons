import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsCoreModule } from 'ngx-angularcommons-core';

import { CommonsCacheService } from './services/commons-cache.service';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsCoreModule
		],
		declarations: [
		],
		exports: [
		]
})
export class NgxAngularCommonsCacheModule {
	static forRoot(): ModuleWithProviders<NgxAngularCommonsCacheModule> {
		return {
				ngModule: NgxAngularCommonsCacheModule,
				providers: [
						CommonsCacheService
				]
		};
	}
}
