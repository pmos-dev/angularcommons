import { Injectable } from '@angular/core';

import { CommonsType } from 'tscommons-core';

import { CommonsStorageService } from 'ngx-angularcommons-core';

import { ICommonsCache } from '../interfaces/icommons-cache';

@Injectable()
export class CommonsCacheService {
	constructor(
			private storageService: CommonsStorageService
	) {}

	private static makeIndex(index: any): string {
		return `angularcommons_cache_${JSON.stringify(index)}`;
	}

	private static makeNs(ns: string, context?: string): string {
		let index: string = `angularcommons_cache:${ns}/`;
		if (context !== undefined) return index = `${index}${context}:`;
		return index;
	}

	private static makeNsIndex(ns: string, context: string, id: any): string {
		return CommonsCacheService.makeNs(ns, context) + JSON.stringify(id);
	}
			
	private internalFetch<T>(internalIndex: string): T|undefined {
		const existing: ICommonsCache = this.storageService.getAny(
				CommonsStorageService.SESSION,
				internalIndex
		);
		if (existing === undefined) return undefined;
		
		if (existing.expiry > -1 && existing.timestamp < (new Date().getTime() - existing.expiry)) {
			this.storageService.delete(
					CommonsStorageService.SESSION,
					internalIndex
			);
			return undefined;
		}
		
		return CommonsType.decode(existing.value) as T;
	}
	
	public fetch<T>(index: any): T|undefined {
		const internalIndex: string = CommonsCacheService.makeIndex(index);
		return this.internalFetch<T>(internalIndex);
	}
	
	public fetchNs<T>(ns: string, context: string, id: any): T|undefined {
		const internalIndex: string = CommonsCacheService.makeNsIndex(ns, context, id);
		return this.internalFetch<T>(internalIndex);
	}
	
	private internalStore<T>(internalIndex: string, value: T, expiry: number): void {
		const cache: ICommonsCache = {
			value: CommonsType.encode(value),
			timestamp: new Date().getTime(),
			expiry: expiry
		};
		
		this.storageService.setAny(
				CommonsStorageService.SESSION,
				internalIndex,
				cache
		);
	}
	
	public store<T>(index: any, value: T, expiry: number): void {
		const internalIndex: string = CommonsCacheService.makeIndex(index);
		return this.internalStore<T>(internalIndex, value, expiry);
	}
	
	public storeNs<T>(ns: string, context: string, id: any, value: T, expiry: number): void {
		const internalIndex: string = CommonsCacheService.makeNsIndex(ns, context, id);
		return this.internalStore<T>(internalIndex, value, expiry);
	}
	
	private internalTouch(internalIndex: string, expiry?: number): boolean {
		const existing: ICommonsCache = this.storageService.getAny(
				CommonsStorageService.SESSION,
				internalIndex
		);
		if (existing === undefined) return false;
		
		existing.timestamp = new Date().getTime();
		if (expiry !== undefined) existing.expiry = expiry;
		
		this.storageService.setAny(
				CommonsStorageService.SESSION,
				internalIndex,
				existing
		);
		
		return true;
	}
	
	public touch(index: any, expiry?: number): boolean {
		const internalIndex: string = CommonsCacheService.makeIndex(index);
		return this.internalTouch(internalIndex, expiry);
	}
	
	public touchNs(ns: string, context: string, id: any, expiry?: number): boolean {
		const internalIndex: string = CommonsCacheService.makeNsIndex(ns, context, id);
		return this.internalTouch(internalIndex, expiry);
	}
	
	private internalDelete(internalIndex: string): boolean {
		const existing: ICommonsCache = this.storageService.getAny(
				CommonsStorageService.SESSION,
				internalIndex
		);
		if (existing === undefined) return false;
		
		this.storageService.delete(
				CommonsStorageService.SESSION,
				internalIndex
		);
		
		return true;
	}
	
	public delete(index: any): boolean {
		const internalIndex: string = CommonsCacheService.makeIndex(index);
		return this.internalDelete(internalIndex);
	}
	
	public deleteNs(ns: string, context: string, id: any): boolean {
		const internalIndex: string = CommonsCacheService.makeNsIndex(ns, context, id);
		return this.internalDelete(internalIndex);
	}
	
	public flushNs(ns: string, context?: string): void {
		const match: string = CommonsCacheService.makeNs(ns, context);
		
		for (const key of Object.keys(CommonsStorageService.SESSION)) {
			if (key.substring(0, match.length) === match) {
				this.storageService.delete(
						CommonsStorageService.SESSION,
						key
				);
			}
		}
	}
}
