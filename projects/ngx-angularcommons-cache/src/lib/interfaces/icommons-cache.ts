import { TEncoded } from 'tscommons-core';

export interface ICommonsCache {
		value: TEncoded;
		timestamp: number;
		expiry: number;
}
