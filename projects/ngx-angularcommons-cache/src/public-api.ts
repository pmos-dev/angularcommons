/*
 * Public API Surface of ngx-angularcommons-cache
 */

export * from './lib/types/tcommons-cache-getter';
export * from './lib/types/tcommons-cache-setter';

export * from './lib/interfaces/icommons-cache';

export * from './lib/enums/ecommons-cache-age';

export * from './lib/helpers/commons-namespaced-sub-cache';

export * from './lib/services/commons-cache.service';

export * from './lib/ngx-angularcommons-cache.module';
