import { Injectable } from '@angular/core';

import { Observable, defer } from 'rxjs';

import { CommonsSynchronised } from 'tscommons-async';
import { ECommonsSynchronisedTimeoutAction } from 'tscommons-async';

@Injectable({
	providedIn: 'root'
})
export class CommonsSynchronisedService {
	private synchroniser: CommonsSynchronised;

	constructor() {
		this.synchroniser = new CommonsSynchronised();
	}

	public async synchronised<T>(
			context: string,
			timeout: number,
			timeoutAction: ECommonsSynchronisedTimeoutAction,
			callback: () => Promise<T>
	): Promise<T> {
		return await this.synchroniser.synchronised(
				context,
				timeout,
				timeoutAction,
				callback
		);
	}

	public synchronisedAsObservable<T>(
			context: string,
			timeout: number,
			timeoutAction: ECommonsSynchronisedTimeoutAction,
			callback: () => Promise<T>
	): Observable<T> {
		return defer(
			async (): Promise<T> => {
				return await this.synchronised<T>(
						context,
						timeout,
						timeoutAction,
						callback
				);
			}
		);
	}
}
