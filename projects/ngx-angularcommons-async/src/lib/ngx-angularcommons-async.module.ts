import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonsSyncIntervalService } from './services/commons-sync-interval.service';
import { CommonsSynchronisedService } from './services/commons-synchronised.service';

@NgModule({
		imports: [
				CommonModule
		],
		declarations: [
		],
		exports: [
		]
})
export class NgxAngularCommonsAsyncModule {
	static forRoot(): ModuleWithProviders<NgxAngularCommonsAsyncModule> {
		return {
				ngModule: NgxAngularCommonsAsyncModule,
				providers: [
						CommonsSyncIntervalService,
						CommonsSynchronisedService
				]
		};
	}
}
