import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';

@Injectable()
export class CommonsConfigService {
	private config: any = {};

	constructor(
			private http: HttpClient
	) {}

	public async load(configPath: string): Promise<void> {
		try {
			this.config = await this.http.get<{}>(configPath).toPromise();
		} catch (error) {
			console.log(error);
		}
	}

	public getDirect(context: string): {}|undefined {
		if (!CommonsType.hasPropertyObject(this.config, context)) return undefined;
		return this.config[context];
	}

	public getAny(context: string, key: string, defaultValue?: any): any|undefined {
		if (
				!CommonsType.hasPropertyObject(this.config, context)
				|| !CommonsType.hasProperty(this.config[context], key)
		) return defaultValue;
		
		return this.config[context][key];
	}

	public getObject<T>(context: string, key: string, defaultValue?: T): T|undefined {
		const value: any|undefined = this.getAny(context, key, defaultValue);

		return CommonsType.attemptObject(value) as T;
	}

	public getString(context: string, key: string, defaultValue?: string): string|undefined {
		const value: any|undefined = this.getAny(context, key, defaultValue);

		return CommonsType.attemptString(value);
	}
		
	public getNumber(context: string, key: string, defaultValue?: number): number|undefined {
		const value: any|undefined = this.getAny(context, key, defaultValue);

		return CommonsType.attemptNumber(value);
	}

	public getBoolean(context: string, key: string, defaultValue?: boolean): boolean|undefined {
		const value: any|undefined = this.getAny(context, key, defaultValue);
		
		return CommonsType.attemptBoolean(value);
	}
}
