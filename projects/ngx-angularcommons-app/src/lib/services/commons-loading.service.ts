import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class CommonsLoadingService {
	private onLoaded: EventEmitter<void> = new EventEmitter<void>(true);

	public loadedObservable(): Observable<void> {
		return this.onLoaded;
	}
	
	public loaded(): void {
		this.onLoaded.emit();
	}
	
}
