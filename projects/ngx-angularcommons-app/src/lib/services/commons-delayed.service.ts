import { Injectable, EventEmitter } from '@angular/core';

import { CommonsConfigService } from './commons-config.service';

@Injectable()
export class CommonsDelayedService {
	private onDelayed: EventEmitter<boolean> = new EventEmitter<boolean>(true);

	delays: string[] = [];

	private delayedTime: number;
	private timedoutTime: number;

	constructor(
		private configService: CommonsConfigService
	) {
		this.delayedTime = (this.configService.getNumber('system', 'delayed', 2) || 2) * 1000;
		this.timedoutTime = (this.configService.getNumber('system', 'timedout', 30) || 30) * 1000;
	}

	public delayedObservable(): EventEmitter<boolean> {
		return this.onDelayed;
	}
	
	public delayed(context: string): void {
		if (-1 === this.delays.indexOf(context)) this.delays.push(context);
		
		if (this.delays.length) this.onDelayed.emit(true);
	}
	
	public done(context: string): void {
		const index: number = this.delays.indexOf(context);
		if (-1 !== index) {
			this.delays.splice(index, 1);
			this.delays = this.delays.slice();	// trigger change detection
		}
		
		if (!this.delays.length) this.onDelayed.emit(false);
	}
	
	public fail(context: string): void {
		this.done(context);
	}
	
	public attempt(
			action: (
					done: () => void,
					fail: () => void
			) => void,
			timeout: (
					fail: () => void
			) => void,
			instantDelayed: boolean = false
	): void {
		const context: string = Math.random().toString();

		const delayTimeout = setTimeout((): void => {
			this.delayed(context);
		}, instantDelayed ? 0 : this.delayedTime);
		
		const timedoutTimeout = setTimeout((): void => {
			clearTimeout(delayTimeout);

			timeout(() => {
				clearTimeout(timedoutTimeout);
				clearTimeout(delayTimeout);
	
				this.fail(context);
			});
		}, this.timedoutTime);
		
		action(() => {
			clearTimeout(timedoutTimeout);
			clearTimeout(delayTimeout);
			
			this.done(context);
		}, () => {
			clearTimeout(timedoutTimeout);
			clearTimeout(delayTimeout);

			this.fail(context);
		});
	}
	
	public attemptWithoutTimeout(
			action: (
					done: () => void,
					fail: () => void
			) => void,
			instantDelayed: boolean = false
	): void {
		const context: string = Math.random().toString();

		const delayTimeout = setTimeout((): void => {
			this.delayed(context);
		}, instantDelayed ? 0 : this.delayedTime);
		
		action(() => {
			clearTimeout(delayTimeout);
			
			this.done(context);
		}, () => {
			clearTimeout(delayTimeout);

			this.fail(context);
		});
	}
}
