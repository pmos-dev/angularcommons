import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonsPrettyIntegerPipe } from './pipes/commons-pretty-integer.pipe';
import { CommonsPrettyPercentagePipe } from './pipes/commons-pretty-percentage.pipe';
import { CommonsPrettyFloatPipe } from './pipes/commons-pretty-float.pipe';
import { CommonsPrettyDatePipe } from './pipes/commons-pretty-date.pipe';
import { CommonsPrettyTimePipe } from './pipes/commons-pretty-time.pipe';
import { CommonsPrettyDateTimePipe } from './pipes/commons-pretty-date-time.pipe';
import { CommonsPrettyDateTimeRangePipe } from './pipes/commons-pretty-date-time-range.pipe';
import { CommonsPrettyLimitLengthPipe } from './pipes/commons-pretty-limit-length.pipe';
import { CommonsPrettyFigurePipe } from './pipes/commons-pretty-figure.pipe';
import { CommonsPrettyOrdinalPipe } from './pipes/commons-pretty-ordinal.pipe';
import { CommonsPrettySizePipe } from './pipes/commons-pretty-size.pipe';
import { CommonsCamelCasePipe } from './pipes/commons-camel-case.pipe';
import { CommonsUcWordsPipe } from './pipes/commons-uc-words.pipe';
import { CommonsPrettyDistancePipe } from './pipes/commons-pretty-distance.pipe';
import { CommonsQuantifyPipe } from './pipes/commons-quantify.pipe';
import { CommonsPrettyDurationPipe } from './pipes/commons-pretty-duration.pipe';

@NgModule({
		imports: [
				CommonModule
		],
		declarations: [
				CommonsCamelCasePipe,
				CommonsPrettyDateTimeRangePipe,
				CommonsPrettyDateTimePipe,
				CommonsPrettyDatePipe,
				CommonsPrettyDistancePipe,
				CommonsPrettyDurationPipe,
				CommonsPrettyFigurePipe,
				CommonsPrettyFloatPipe,
				CommonsPrettyIntegerPipe,
				CommonsPrettyLimitLengthPipe,
				CommonsPrettyOrdinalPipe,
				CommonsPrettyPercentagePipe,
				CommonsPrettySizePipe,
				CommonsPrettyTimePipe,
				CommonsQuantifyPipe,
				CommonsUcWordsPipe
		],
		exports: [
				CommonsCamelCasePipe,
				CommonsPrettyDateTimeRangePipe,
				CommonsPrettyDateTimePipe,
				CommonsPrettyDatePipe,
				CommonsPrettyDistancePipe,
				CommonsPrettyDurationPipe,
				CommonsPrettyFigurePipe,
				CommonsPrettyFloatPipe,
				CommonsPrettyIntegerPipe,
				CommonsPrettyLimitLengthPipe,
				CommonsPrettyOrdinalPipe,
				CommonsPrettyPercentagePipe,
				CommonsPrettySizePipe,
				CommonsPrettyTimePipe,
				CommonsQuantifyPipe,
				CommonsUcWordsPipe
		]
})
export class NgxAngularCommonsPipeModule { }
