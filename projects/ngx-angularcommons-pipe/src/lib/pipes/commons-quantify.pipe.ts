import { Pipe, PipeTransform } from '@angular/core';

import { CommonsString } from 'tscommons-core';

@Pipe({
		name: 'commonsQuantify'
})
export class CommonsQuantifyPipe implements PipeTransform {

	transform(singular: string|any, enabled: boolean = true, quantity: number = 2, autoDetectEndings: boolean = true, capitaliseNoNone: boolean = false): any {
		if (!enabled) return singular;
		
		if (singular === undefined) return '';

		return CommonsString.quantify(singular, quantity, autoDetectEndings, capitaliseNoNone);
	}

}
