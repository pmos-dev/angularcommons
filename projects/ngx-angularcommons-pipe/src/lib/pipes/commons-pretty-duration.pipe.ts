import { Pipe, PipeTransform } from '@angular/core';

import { CommonsType, CommonsNumber } from 'tscommons-core';

@Pipe({
		name: 'commonsPrettyDuration'
})
export class CommonsPrettyDurationPipe implements PipeTransform {

	transform(value: number|any, enabled: boolean = true, short: boolean = false): string|any {
		if (!enabled) return value;
		
		if (!CommonsType.isNumber(value)) return '-';
		
		return CommonsNumber.prettyDuration(value, short);
	}

}
