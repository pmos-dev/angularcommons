import { Pipe, PipeTransform } from '@angular/core';

import { CommonsType } from 'tscommons-core';
import { CommonsDate } from 'tscommons-core';

@Pipe({
		name: 'commonsPrettyDate'
})
export class CommonsPrettyDatePipe implements PipeTransform {

	transform(
			value: Date|unknown,
			enabled: boolean = true,
			absolute?: boolean,
			now?: Date,
			includeYear?: boolean,
			iso?: boolean
	): string|Date {
		if (!CommonsType.isDate(value)) return '-';

		if (!enabled) return value;
		
		return CommonsDate.prettyDate(
				value,
				absolute,
				now,
				includeYear,
				iso
		);
	}

}
