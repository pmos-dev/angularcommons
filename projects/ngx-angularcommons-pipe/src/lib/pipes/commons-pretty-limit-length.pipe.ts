import { Pipe, PipeTransform } from '@angular/core';

import { CommonsString } from 'tscommons-core';

@Pipe({
	name: 'commonsPrettyLimitLength'
})
export class CommonsPrettyLimitLengthPipe implements PipeTransform {

	transform(value: string|any, enabled: boolean = true, length: number = 128, soft: boolean = false): string|any {
		if (!enabled) return value;
		
		return CommonsString.limitLength(value, length, soft);
	}

}
