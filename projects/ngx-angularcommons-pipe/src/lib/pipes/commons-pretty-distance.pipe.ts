import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
		name: 'commonsPrettyDistance'
})
export class CommonsPrettyDistancePipe implements PipeTransform {

	transform(value: number|any, enabled: boolean = true): any {
		if (!enabled) return value;
		
		if (value === undefined) return '-';
		if (isNaN(value)) return '-';

		let unit: string = 'm';
		let divisor: number = 1;
		if (value >= 1000) {
			unit = 'km';
			divisor = 1000;
		}
		
		return `${parseFloat((value / divisor).toPrecision(3)).toLocaleString()}${unit}`;
	}

}
