import { Pipe, PipeTransform } from '@angular/core';

import { CommonsNumber } from 'tscommons-core';

@Pipe({
	name: 'commonsPrettyPercentage'
})
export class CommonsPrettyPercentagePipe implements PipeTransform {

	transform(value: number|any, enabled: boolean = true, whole: boolean = false): string|number|any {
		if (!enabled) return value;
		
		if (isNaN(value)) return '-';

		return CommonsNumber.prettyPercent(value, whole);
	}

}
