import { Pipe, PipeTransform } from '@angular/core';

import { CommonsNumber } from 'tscommons-core';

@Pipe({
		name: 'commonsPrettySize'
})
export class CommonsPrettySizePipe implements PipeTransform {

	transform(value: number|any, enabled: boolean = true, binaryPrefix: boolean = false): any {
		if (!enabled) return value;
		
		if (value === undefined) return '-';

		return CommonsNumber.prettyFileSize(value!, binaryPrefix);
	}

}
