import { Pipe, PipeTransform } from '@angular/core';

import { CommonsType } from 'tscommons-core';
import { CommonsDate } from 'tscommons-core';

// This pipe is odd in that it needs two variables, so uses an array as input

@Pipe({
		name: 'commonsPrettyDateTimeRange'
})
export class CommonsPrettyDateTimeRangePipe implements PipeTransform {
	transform(
			values: Date[]|unknown,
			enabled: boolean = true,
			absolute?: boolean,
			now?: Date,
			includeYear?: boolean,
			iso?: boolean
	): string|Date[] {
		if (!CommonsType.isDateArray(values)) return '-';

		if (!enabled) return values;

		return CommonsDate.prettyDateTimeRange(
				{
						from: values[0],
						to: values[1]
				},
				absolute,
				now,
				includeYear,
				iso
		);
	}

}
