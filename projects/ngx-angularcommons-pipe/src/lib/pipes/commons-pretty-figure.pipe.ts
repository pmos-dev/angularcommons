import { Pipe, PipeTransform } from '@angular/core';

import { CommonsNumber } from 'tscommons-core';

@Pipe({
		name: 'commonsPrettyFigure'
})
export class CommonsPrettyFigurePipe implements PipeTransform {

	transform(value: number|any, enabled: boolean = true, absolute: boolean = false): any {
		if (!enabled) return value;
		
		if (value === undefined) return '-';

		return CommonsNumber.prettyFigure(value!, absolute);
	}

}
