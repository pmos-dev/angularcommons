import { Pipe, PipeTransform } from '@angular/core';

import { CommonsString } from 'tscommons-core';

@Pipe({
		name: 'commonsCamelCase'
})
export class CommonsCamelCasePipe implements PipeTransform {

	transform(value: string|any, enabled: boolean = true, ucFirst: boolean = true): any {
		if (!enabled) return value;
		
		if (value === undefined) return '';

		return CommonsString.camelCase(value, ucFirst);
	}

}
