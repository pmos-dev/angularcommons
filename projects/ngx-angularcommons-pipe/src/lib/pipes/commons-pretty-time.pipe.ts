import { Pipe, PipeTransform } from '@angular/core';

import { CommonsType } from 'tscommons-core';
import { CommonsDate } from 'tscommons-core';

@Pipe({
		name: 'commonsPrettyTime'
})
export class CommonsPrettyTimePipe implements PipeTransform {

	transform(
			value: Date|unknown,
			enabled: boolean = true,
			absolute?: boolean,
			now?: Date,
			iso?: boolean
	): string|Date|any {
		if (!CommonsType.isDate(value)) return '-';

		if (!enabled) return value;

		return CommonsDate.prettyTime(
				value,
				absolute,
				now,
				iso
		);
	}
	
}
