import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgxAngularCommonsCoreModule } from 'ngx-angularcommons-core';
import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';
import { NgxAngularCommonsAppModule } from 'ngx-angularcommons-app';

import { AppComponent } from './app.component';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		NgxAngularCommonsCoreModule.forRoot(),
		NgxAngularCommonsPipeModule,
		NgxAngularCommonsAppModule.forRoot()
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
