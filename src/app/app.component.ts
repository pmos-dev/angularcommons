import { Component } from '@angular/core';

import { CommonsConfigService } from 'ngx-angularcommons-app';

@Component({
	selector: 'commons-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.less']
})
export class AppComponent {
	title = 'AngularCommons';
	
	constructor(
			configService: CommonsConfigService
	) {
		console.log(configService.getString('storage', 'namespace'));
	}
}
